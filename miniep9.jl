function multiplica(a, b)
    dima = size(a)
    dimb = size(b)
    if dima[2] != dimb[1]
        return -1
    end
    c = zeros(dima[1], dimb[2])
    for i in 1:dima[1]
        for j in 1:dimb[2]
            for k in 1:dima[2]
                c[i, j] = c[i, j] + a[i, k] * b[k, j]
            end
        end
    end
    return c
end

function matrix_pot(M, p)
    if p == 1
        return M
    end
    if p == 2
        return multiplica(M, M)
    end
    x = multiplica(M, M)
    for i in 1:p-2
        x = multiplica(x, M)
    end
    return x
end

function matrix_pot_by_squaring(M, p)
    if p < 0
        return matrix_pot_by_squaring(1 / M, -p)
    elseif p == 0
        return 1
    elseif p == 1
        return M
    elseif p % 2 == 0
        return matrix_pot_by_squaring(multiplica(M, M), p / 2)
    elseif p % 2 == 1
        return multiplica(M, matrix_pot_by_squaring(multiplica(M, M), (p - 1) / 2))
    end
end

using LinearAlgebra
function compare_times()
    M = Matrix(LinearAlgebra.I, 30, 30)
    print("tempo da matrix_pot =") 
    @time matrix_pot(M, 10)
    print("tempo da matrix_pot_by_squaring =") 
    @time matrix_pot_by_squaring(M, 10)
end

function teste()
    println(matrix_pot([1 2 ; 3 4], 1))
    println(matrix_pot([1 2 ; 3 4], 2))
    println(matrix_pot([4 8 0 4 ; 8 4 9 6 ; 9 6 4 0 ; 9 5 4 7], 7))
    println(matrix_pot_by_squaring([1 2 ; 3 4], 1))
    println(matrix_pot_by_squaring([1 2 ; 3 4], 2))
    println(matrix_pot_by_squaring([4 8 0 4 ; 8 4 9 6 ; 9 6 4 0 ; 9 5 4 7], 7))
    println("Final dos Testes")
end

teste()
compare_times()
