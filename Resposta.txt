matrix_pot = 0.000450 seconds (9 allocations: 64.688 KiB)       
matrix_pot_by_squaring = 0.000210 seconds (7 allocations: 28.969 KiB) 

Houve diferença nos tempos de execução? Por quê?

R: Sim, a função matrix_pot demorou mais do que o dobro do tempo da função matrix_pot_by_squaring para ser concluída, 
podendo ser explicado pelo fato da matrix_pot executar todas as "p - 1" multiplicações de matrizes enquanto a função 
matrix_pot_by_squaring realiza suas contas reduzindo a potência em "p / 2" a cada recursão realizada, ou seja, 
aumentando cada vez mais a diferença entre os tempos com o aumentar da potência.
